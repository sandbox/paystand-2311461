commerce_paystand README

The commerce_paystand module adds a Drupal Commerce payment gateway for
PayStand.

Installation

1. You will need your Organization Id (org_id) and your Public API Key, found
on your PayStand dashboard under Settings > API.
2. Download the commerce_paystand.tgz or commerce_paystand.zip package and
extract it into the modules directory of your Drupal site.
3. Enable the module in your Drupal admin page under
Site > Configuration > Modules.  Then enable the offsite sub-module.
4. In your Drupal Commerce Payment Settings page, enable the PayStand payment
option and edit the rule settings using your org_id and Public API key.
5. Edit this Drupal Callback Url by replacing 'example.com' with your site's
address:
http://example.com/commerce_paystand/psn/commerce_paystand_offsite/
commerce_payment_commerce_paystand_offsite
Note that the above url is broken into two lines to satisfy drupal module
review and should be combined into one line.
6. Insert your edited Callback Url into your PayStand dashboard under
Settings > Checkout Features > Webhook Url.
7. Go shopping on your Drupal site and pay with PayStand!

If you would like to use our sandbox environment for testing, please contact us
for additional details.

This is the first version of this module. If you have questions or feedback,
please email us at support@paystand.com.
